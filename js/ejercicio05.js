/* Regresa un arreglo con la suma de los números de cada uno de los arreglos anidados que conforman el arreglo de entrada.*/
function ejercicio05(arreglo) {
    var numeros=[];
 for(var i=0; i<arreglo.length; i++)
 {
     var sum=0;
     for(var j=0;j<arreglo[i].length;j++)
     {
         sum+=arreglo[i][j];
     }
     numeros.push(sum);
 }
 return numeros;
}


